# Using Redis with Node.js

**Write a Node.js application that**

1. receives HTTP POST requests only on a "/track" route
    * gets data in JSON format passed in the request body
    * saves the JSON data into a local file (append)
    * if the data contains a "count" parameter, the application increments the value of the "count" key by the value of the 'count' parameter in a Redis database
2. receives HTTP GET requests only on a "/count" route
    * returns the value of the "count" key from the Redis database
3. Write appropriate unit tests.

## Table of Contents

* [Getting Started](#getting-started)
  * [Prerequisite](#prerequisite)
  * [Installation](#installation)
* [Running](#running)
    * [Example Request](#example-request)
* [Built with](#built-with)
  * [Development Environment](#development-environment)
* [Authors](#authors)
* [License](#license)
* [Resources](#resources)

## Getting Started

These instructions will guide you to up and running the project on your local machine.

### Prerequisite

* Node.js
* NPM
* Yarn

### Installation

Now you can run ```yarn install``` to install dependencies.

## Running

You can start the server with `yarn server`. Below you may find route list.

| Method  | Path                         |
| ------  | ---------------------------  |
| GET     | [/count](http://localhost:3000/count) |
| GET     | [/count/del](http://localhost:3000/count/del) |
| POST    | [/track](http://localhost:3000/graphql) |

At this point if you can access to [/count](http://localhost:3000/count), you will see that page contents only zero(0). 

When you want 'count' to be zero again then you can simply access to [/count/del](http://localhost:3000/count/del).

### Examples Request

**cmd:**
```sh
curl -X POST http://localhost:3000/track \
  -H "Content-Type: application/json" \
  -d '{"count": 3}'
```

**Response:**
```json
{
  "success": true   
}
```

**cmd:**
```sh
curl -X POST http://localhost:3000/track \
  -H "Content-Type: application/json"
```

**Response:**
```json
{
  "success": false, 
  "error": "Data is not JSON"
}
```

## Testing

You can run tests with `yarn test`.

## Built with

#### Development Environment
* [Ubuntu](https://www.ubuntu.com/download/server) - Server
* [Linux Mint 18](https://www.linuxmint.com/) - OS
* [WebStorm](https://www.jetbrains.com/webstorm/) - IDE
* [Chromium](https://www.chromium.org/Home) - Browser

## Authors

* **Emrah Sifoğlu** - *Initial work* - [emrahsifoglu](https://github.com/emrahsifoglu)

## License

This project is licensed under the [MIT License](http://opensource.org/licenses/MIT).

## Resources

- http://brianflove.com/2016/11/08/typescript-2-express-node/
- http://brianflove.com/2016/11/11/typescript-2-express-mongoose-mocha-chai/
- https://blog.pusher.com/use-typescript-with-node/
- https://basarat.gitbooks.io/typescript/docs/quick/nodejs.html
- https://codeforgeek.com/2016/06/node-js-redis-tutorial-installation-commands/
- https://community.risingstack.com/redis-node-js-introduction-to-caching/
- https://devhints.io/chai
- https://linode.com/docs/databases/redis/how-to-install-a-redis-server-on-ubuntu-or-debian8/
- https://inviqa.com/blog/using-typescript-node-js
- https://stackoverflow.com/questions/6528876/how-to-redirect-404-errors-to-a-page-in-expressjs
- https://stackoverflow.com/questions/44134213/chai-node-js-expectvalue-to-be-nan-issue
- https://stackoverflow.com/questions/39030562/chai-expectres-body-fails-consistently
- https://www.youtube.com/watch?v=7pvk_zflkwU
- https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-redis-on-ubuntu-16-04
- https://www.sitepoint.com/using-redis-node-js/
- https://www.tutorialkart.com/nodejs/node-js-append-data-to-file/
- https://journal.artfuldev.com/write-tests-for-typescript-projects-with-mocha-and-chai-in-typescript-86e053bdb2b6
- http://mherman.org/blog/2016/11/05/developing-a-restful-api-with-node-and-typescript/#.WpfwKHVubQ0
- https://github.com/bradtraversy/redusers
