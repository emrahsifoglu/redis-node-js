import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import { ServerRoutes } from './routes/serverRoutes';

class Server {

    public app: express.Application;

    constructor() {
        this.app = express();
        this.middleware();
        this.routes();
    }

    public middleware(): void {
        this.app.use(cors());
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: true }));
    }

    private routes(): void {
        this.app.use('/', ServerRoutes);
    }
}

export default new Server().app;
