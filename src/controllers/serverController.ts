import { Request, Response } from 'express';
import * as redis from 'redis';
import * as fs from 'fs';

const client = redis.createClient();

client.on('connect', function() {
    console.log('Connected to Redis...');
});

function appendFile(data: string) {
    fs.appendFile('./assets/data.txt', data, 'utf8', function(err: any) {
        if (err) throw err;
        console.log("Data is appended to file successfully.")
    });
}

export const helloWord = (req: Request, res: Response) => {
    res.json({
        message: 'Hello World!'
    });
};

export const track = (req: Request, res: Response) => {
    let err = 'Data is not JSON';
    let body = req.body;

    if(JSON.stringify(body) === '{}') {
        return res.json({
            success: false,
            error: err
        });
    }

    if (body.count) {
        client.get('count', function (err: any, reply: any) {
            if (err) throw err;

            let count: number = 0;

            if (reply != null) {
                count += parseInt(reply);
            }

            let value = parseInt(req.body.count) + count;

            client.set('count', value.toString());
        });
    }

    let data = JSON.stringify(body) + '\n';

    appendFile(data);

    res.json({
        success: true
    });
};

export const count = (req: Request, res: Response) => {
    client.get('count', function (err: any, reply: any) {
        if (err) throw err;

        if (reply != null) {
            return res.send(reply);
        } else {
            return res.send('0');
        }
    });
};

export const countDel =  (req: Request, res: Response) => {
    client.set('count', '0');
    res.send("count is zero");
};

export const notFound = (req: Request, res: Response) => {
    res.send('404');
};
