import { Router } from 'express';
import * as  serverController from '../controllers/serverController';

const router: Router = Router();

router.get('/', serverController.helloWord);
router.post('/track', serverController.track);
router.get('/count', serverController.count);
router.get('/count/del', serverController.countDel);
router.get('*', serverController.notFound);

export const ServerRoutes: Router = router;
