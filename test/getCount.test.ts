import * as chai from 'chai';
import chaiHttp = require('chai-http');

import app from '../src/Server';

chai.use(chaiHttp);
const expect = chai.expect;

describe('countRoute', () => {

    it('should be html', () => {
        return chai.request(app).get('/count')
            .then(res => {
                expect(res.type).to.eql('text/html');
            });
    });

    it('should have a message prop', () => {
        return chai.request(app).get('/count')
            .then(res => {
                expect(res.text).to.not.NaN;
            });
    });

    it('should have at least zero', () => {
        return chai.request(app).get('/count')
            .then(res => {
                expect(parseInt(res.text)).to.be.gte(0);
            });
    });

});
